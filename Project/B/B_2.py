from mrjob.job import MRJob


class MRKiarash(MRJob):
    def mapper(self, _, line):
        data = line.split()
        if data[0][1] == 'B':
            yield int(data[0][2:-1]), ('c', data[1])
        else:
            yield int(data[0][1:-1]), ('q', data[1])

    def reducer(self, key, values):
        q = 0
        c = 0
        for val in values:
            if val[0] == 'q':
                q = val[1]
            if val[0] == 'c':
                c = val[1]
        if int(q) == 0:
            ctr = 0
        else:
            ctr = int(c)/int(q)
        yield (key, ctr)


if __name__ == '__main__':
    MRKiarash.run()
