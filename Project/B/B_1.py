from mrjob.job import MRJob


class MRKiarash(MRJob):

    def mapper(self, _, line):
        data = line.split()
        if data[2] == 'C':
            yield data[3], 1
        else:
            pass

    def reducer(self, key, values):
        sums = sum(values)
        yield ("B"+key, sums)


if __name__ == '__main__':
    MRKiarash.run()
