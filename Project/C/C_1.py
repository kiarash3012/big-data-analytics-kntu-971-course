from mrjob.job import MRJob


class MRKiarash(MRJob):
    def mapper(self, _, line):
        data = line.split()
        if data[2] == 'Q':
            q_list = ['q', data[1]]
            for i in range(6, 11):
                q_list.append(data[i])
            yield data[0], q_list
        else:
            yield data[0], ['c', data[3]]

    def combiner(self, key, values):
        c_list = ['c']
        for val in values:
            if val[0] == 'q':
                yield key, val
            if val[0] == 'c':
                for elem in range(1, len(val)):
                    c_list.append(val[elem])
        yield key, c_list

    def reducer(self, key, values):
        values_list = list()
        c_list = list()
        for val in values:
            values_list.append(val)
            if val[0] == 'c':
                c_list = val[1:]

        for val in values_list:
            print(val)
            if val[0] == 'q':
                pattern = ''
                for elem in val[2:]:
                    if elem in c_list:
                        pattern += '1'
                    else:
                        pattern += '0'
                for elem in range(2, len(val)):
                    out_put = [val[elem], elem, pattern]
                    yield val[1], out_put


if __name__ == '__main__':
    MRKiarash.run()
