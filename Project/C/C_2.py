from mrjob.job import MRJob


class MRKiarash(MRJob):
    def mapper(self, _, line):
        data = line
        data = data.replace('"', '')
        data = data.replace('[', '')
        data = data.replace(']', '')
        data = data.replace(',', '')
        data = data.split()

        yield [data[0], data[1], data[2]], data[3]

    def reducer(self, key, values):
        patterns_list = dict()

        for val in values:
            if val not in patterns_list:
                patterns_list[val] = 1
            elif val in patterns_list:
                patterns_list[val] += 1
        yield key, patterns_list


if __name__ == '__main__':
    MRKiarash.run()
