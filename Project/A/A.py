from mrjob.job import MRJob


class MRKiarash(MRJob):

    def mapper(self, _, line):
        data = line.split()
        if data[2] == 'Q':
            for i in range(5, len(data)-5):
                yield data[i], 1
        else:
            pass

    def reducer(self, key, values):
        sums = sum(values)
        yield (key, sums) 


if __name__ == '__main__':
    MRKiarash.run()
