app_1.py is the answer to the 3_1_1 and the output is in app_1
app_2.py is the answer to the 3_1_2 and the output is in app_2 (it will find n best-selling items, default n is 10)

sample command to run .py files:
python app_2.py customer-orders.csv --output-dir app_2