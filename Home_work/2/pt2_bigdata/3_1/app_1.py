from mrjob.job import MRJob


class MRKiarash(MRJob):

    def mapper(self, _, line):
        data = line.split(',')
        yield (data[0], data[2])

    def reducer(self, key, values):
        sums = sum(values)
        yield (key, sums)


if __name__ == '__main__':
    MRKiarash.run()
