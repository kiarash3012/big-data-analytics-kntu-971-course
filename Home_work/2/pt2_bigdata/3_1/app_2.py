from mrjob.job import MRJob
from mrjob.step import MRStep


class MRKiarash(MRJob):
    def steps(self):
        return [
            MRStep(mapper=self.mapper,
                   reducer=self.reducer),
            MRStep(reducer=self.secondreducer)
        ]

    def mapper(self, _, line):
        data = line.split(',')
        # print(data[1], data[2])
        yield (data[1], data[2])

    def reducer(self, key, values):
        total = 0
        for val in values:
            float_val = float(val)
            total += float_val
        yield None, (key, total)

    def secondreducer(self, _, values):
        n = 10
        n_largest_dict = dict()
        for val in values:
            if len(n_largest_dict) < n:
                n_largest_dict[val[0]] = val[1]
                continue
            count = 0
            for elem in n_largest_dict:
                if val[1] > float(n_largest_dict[elem]):
                    count += 1
            if count == n:
                smallest_item = ''
                for elem in n_largest_dict:
                    for elem2 in n_largest_dict:
                        if n_largest_dict[elem] < n_largest_dict[elem2]:
                            if smallest_item != '':
                                if n_largest_dict[elem] < n_largest_dict[smallest_item]:
                                    smallest_item = elem
                            else:
                                smallest_item = elem

                if smallest_item != '':
                    n_largest_dict.pop(smallest_item, None)
                    n_largest_dict[val[0]] = val[1]
        for elem in n_largest_dict:
            yield elem, n_largest_dict[elem]


if __name__ == '__main__':
    MRKiarash.run()
