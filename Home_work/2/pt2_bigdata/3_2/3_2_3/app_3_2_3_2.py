from mrjob.job import MRJob
from mrjob.step import MRStep


class MRKiarash(MRJob):
    def steps(self):
        return [
            MRStep(mapper=self.mapper,
                   reducer=self.reducer),
        ]

    def mapper(self, _, line):
        data = line.split('|')
        if data[0][0] == 'k':
            str(data[0]).strip()
            data[0] = data[0][1:]
            yield int(data[0]), (data[1], 'rate')
        else:
            yield int(data[0]), (data[1], 'name')

    def reducer(self, key, values):
        rate = None
        name = None
        print('@'*20)
        for val in values:
            print(val)
            if val[1] == 'rate':
                rate = val[0]
            elif val[1] == 'name':
                name = val[0]
        if rate is not None and name is not None:
            yield key, (name, rate)


if __name__ == '__main__':
    MRKiarash.run()
