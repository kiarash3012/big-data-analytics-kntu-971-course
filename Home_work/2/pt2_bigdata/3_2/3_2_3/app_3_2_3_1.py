from mrjob.job import MRJob
from mrjob.step import MRStep


class MRKiarash(MRJob):
    def steps(self):
        return [
            MRStep(mapper=self.mapper,
                   reducer=self.reducer),
        ]

    def mapper(self, _, line):
        data = line.split()
        yield data[1], data[2]

    def reducer(self, key, values):
        sums = 0
        count = 0
        for val in values:
            sums += int(val)
            count += 1
        avg = sums/count

        yield str('k' + str(key)), str('|' + str(avg))


if __name__ == '__main__':
    MRKiarash.run()
