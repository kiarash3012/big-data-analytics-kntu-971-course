from mrjob.job import MRJob
from mrjob.step import MRStep


class MRKiarash(MRJob):
    def steps(self):
        return [
            MRStep(mapper=self.mapper,
                   reducer=self.reducer),
            MRStep(reducer=self.secondreducer)
        ]

    def mapper(self, _, line):
        data = line.split()
        yield data[0], 1

    def reducer(self, key, values):
        sums = sum(values)
        yield None, (key, sums)

    def secondreducer(self, _, values):
        top_user_id = ''
        top_user_votes = ''
        for val in values:
            if top_user_id == '':
                top_user_id = val[0]
                top_user_votes = val[1]
            if top_user_votes < val[1]:
                top_user_id = val[0]
                top_user_votes = val[1]
        yield top_user_id, top_user_votes


if __name__ == '__main__':
    MRKiarash.run()
