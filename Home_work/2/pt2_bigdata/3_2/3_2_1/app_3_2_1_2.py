from mrjob.job import MRJob
from mrjob.step import MRStep


class MRKiarash(MRJob):
    def steps(self):
        return [
            MRStep(mapper=self.mapper,
                   reducer=self.reducer),
        ]

    def mapper(self, _, line):
        data = line.split('|')
        if data[0] == 'null':
            yield data[1], (data[2], 'views')
        else:
            print(data)
            yield data[0], (data[1], 'name')

    def reducer(self, key, values):
        views = None
        name = None
        for val in values:
            if val[1] == 'views':
                views = val[0]
            elif val[1] == 'name':
                name = val[0]
        if views is not None and name is not None:
            yield key, (name, views)


if __name__ == '__main__':
    MRKiarash.run()
