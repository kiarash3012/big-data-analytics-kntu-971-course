from mrjob.job import MRJob
from mrjob.step import MRStep


class MRKiarash(MRJob):
    def steps(self):
        return [
            MRStep(mapper=self.mapper,
                   reducer=self.reducer),
            MRStep(reducer=self.secondreducer)
        ]

    def mapper(self, _, line):
        data = line.split()
        yield data[1], 1

    def reducer(self, key, values):
        sums = sum(values)
        yield None, (key, sums)

    def secondreducer(self, _, values):
        views = 0
        movie_id = ''
        for val in values:
            if val[1] > views:
                views = val[1]
                movie_id = val[0]
        output = '|' + str(movie_id) + '|' + str(views)
        yield None, output


if __name__ == '__main__':
    MRKiarash.run()
