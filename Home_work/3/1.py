from mrjob.job import MRJob
# from mr3px.csvprotocol import CsvProtocol


class MRKiarash(MRJob):

    def mapper(self, _, line):
        data = line.split()
        yield data[1], data[2]
        

    def reducer(self, key, values):
        count = 0
        rate = 0.0
        for val in values:
            count += 1 
            rate += val[2]

        rate = rate/count

        if rate >= 4.0 and count >= 20:
        	yield 'm'+key



if __name__ == '__main__':
    MRKiarash.run()
