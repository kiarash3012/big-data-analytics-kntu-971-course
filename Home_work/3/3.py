from mrjob.job import MRJob
# from mr3px.csvprotocol import CsvProtocol


class MRKiarash(MRJob):
	def jaccard_similarity(list1, list2):
	    intersection = len(list(set(list1).intersection(list2)))
	    union = (len(list1) + len(list2)) - intersection
	    return float(intersection / union)

    def mapper(self, _, line):
        data = line.split('|')
        yield None, [data[0], data[5], data[6], data[7], data[8], data[9], data[10], data[11]]
        

    def reducer(self, key, values):
        _values = list()
        for val in values:
        	_values.append(val)
        
        out_put_list = list()
        for val in _values:
            for elem in _values:
	            if elem[0] != val[0:
                    j_val = jaccard_similarity(elem[1:7], val[1:7]) 
					if j_val > 0.9:
						yield val[0], [elem[0], j_val]


if __name__ == '__main__':
    MRKiarash.run()


