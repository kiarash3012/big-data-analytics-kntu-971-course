from mrjob.job import MRJob
from sklearn.metrics import jaccard_similarity_score

# from mr3px.csvprotocol import CsvProtocol


class MRKiarash(MRJob):
    def jaccard_similarity(list1, list2):
        intersection = len(list(set(list1).intersection(list2)))
        union = (len(list1) + len(list2)) - intersection
        return 1 - float(intersection / union)

    def mapper(self, _, line):
        data = line.split('|')
        print(len(data))
        yield None, [data[0], data[5], data[6], data[7], data[8], data[9], data[10], data[11], data[12], data[13],
                     data[14], data[15], data[16], data[17], data[18], data[19], data[20], data[21], data[22], data[23]]
        
    def reducer(self, key, values):
        _values = list()
        for val in values:
            _values.append(val)
        out_put_list = list()
        for val in _values:
            if int(val[0]) == 50:
                for elem in _values:
                    if elem[0] != val[0]:
                        # j_val = MRKiarash.jaccard_similarity(elem[1:7], val[1:7])
                        j_val = jaccard_similarity_score(elem[1:23], val[1:23])
                        if j_val > 0.9:
                            print('here')
                            yield val[0], elem[0]


if __name__ == '__main__':
    MRKiarash.run()


