from mrjob.job import MRJob
import datatime.datatime as datatime

# from mr3px.csvprotocol import CsvProtocol


class MRKiarash(MRJob):

    def mapper(self, _, line):
        data = line.split()
        if data[0][0] == 'm':
            # movie_id
            yield None, [data[0], '+']
        # movie_id, data
        yield None, [data[1], data[3]]
        

    def reducer(self, key, values):
        
        out_put_list = list()
        for val in values:
            if val[1] == '+':
                for item in values
                    if item[0] == val[0]:
                        out_put_list.append([datatime(item[1]), item[0]]) 

    	yield out_put_list.sort()



if __name__ == '__main__':
    MRKiarash.run()
