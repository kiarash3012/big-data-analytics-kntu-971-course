# Big Data Analysis 971 [kntu ai department](https://ai.ce.kntu.ac.ir/) 
### This repo contains all home works plus project of [Dr.khaste](https://wp.kntu.ac.ir/khasteh/) Big Data analysis course in kntu i used [Mrjob](https://mrjob.readthedocs.io/) as hadoop simulator so you can pick exact logic and deploy it to hadoop results will be the same. 
***
### How to run:
#### 1- pip install mrjob
#### 2- cd to .py file dir
#### 3- python 'python_file.py' 'input_file_for_py_file' --output-dir 'name_of_desirable_output_dir'
#### ex: pythonA.py dataset.txt --output-dir results
***
## Please don't call me for farther instructions on this repo i don't have the time, sorry ;)